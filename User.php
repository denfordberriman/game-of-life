<?php
require "DbConfig.php";
class User extends DbConfig {

    protected $salt = "Salty ";

    public function __construct() {
        parent::__construct();
    }

    public function check_login($username, $password) {

        // create hash
        $password = hash('sha256', $this->salt.$password);
        
        // query for user
        $sql = "SELECT id, username, created FROM user WHERE username = '$username' AND password = '$password'";
        $query = $this->connection->query($sql);
        
        // respond
        if($query->num_rows > 0){
            // valid login
            $row = $query->fetch_array();
            return $row;
        } else {
            // invalid login
            return false;
        }
    }

    public function clean($input) {
        
        // clean the input - simple string
        $input = trim($input);
        $input = addslashes($input);
        $input = strip_tags($input);
        return $input;

    }

}
