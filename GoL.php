<?php
# Rules
# The universe of the Game of Life is an infinite, two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, live or dead, (or populated and unpopulated, respectively). Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:

# 1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
# 2. Any live cell with two or three live neighbours lives on to the next generation.
# 3. Any live cell with more than three live neighbours dies, as if by overpopulation.
# 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
# These rules, which compare the behavior of the automaton to real life, can be condensed into the following:

# A. Any live cell with two or three live neighbours survives.
# B. Any dead cell with three live neighbours becomes a live cell.
# C. All other live cells die in the next generation. Similarly, all other dead cells stay dead.
# -------------------------------------------------------------------------------------------
# https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules

// class for cells
class Cell {
    public $state = false;  //boolean
    public $nextState = false; //boolean to store state before next frame
    public $location = []; // x, y coords of cell
    public $neighbours = 0;

    function __construct() {
        //self::neighbours();
    } 
}

class Universe {
    public static $size = 10; // size of our x & y universe dimensions
    public static $frameRate = 10;
    public static $cells = [];
    public $iteration = 1;
    public static $death = false;
    private static $debug = false;
    
    // create a world and prpeare it's cells
    public function createUniverse() {
        // generate a grid of n x n cells as our universe - top down
        for ($y = 0; $y < self::$size; $y++) {
            for ($x = 0; $x < self::$size; $x++) {
                $c = new Cell;
                $c->location = [$x, $y];
                $c->state = (bool)rand(0,1);
                
                self::$cells[] = $c;
            }
        }
    }

    public function getUniverseHTML() {
        $u = "";
        foreach(self::$cells as $k => $v) {
            $state = 'dead';
            $coords = '';
            if (self::$debug) {
                $coords = $v->location[0].":".$v->location[1];
            }
            if ($v->state) {
                $state = 'alive';
            }

            $u = $u."<div class='cell $state' >$coords</div>";
        }
        return $u;
    }

    public function runSimulation() {
        $u = "";
        foreach(self::$cells as $k => $v) {
            
            // end all life randomly
            if (self::$death) {
                $v = self::heatDeathOfTheUniverse($v);
            }
            
            // neighbours alive
            self::$cells[$k]->neighbours = 0;
            $count = 0;

            // our x, y
            $x = $v->location[0];
            $y = $v->location[1];
            
            // get the surrounding 8
            /**
             *  x|x|x
             *  x|o|x
             *  x|x|x
             */

            // top-left
            $nx = $x - 1;
            $ny = $y - 1;
            if (self::isNeighbourAlive($nx, $ny)) {
                $count++;
            }
            
            // top-mid
            $nx = $x;
            $ny = $y - 1;
            if (self::isNeighbourAlive($nx, $ny)) {
                $count++;
            }
            
            // top-right
            $nx = $x + 1;
            $ny = $y - 1;
            if (self::isNeighbourAlive($nx, $ny)) {
                $count++;
            }
            
            // mid-left
            $nx = $x - 1;
            $ny = $y;
            if (self::isNeighbourAlive($nx, $ny)) {
                $count++;
            }
           
            // mid-right
            $nx = $x + 1;
            $ny = $y;
            if (self::isNeighbourAlive($nx, $ny)) {
                $count++;
            }
            
            // bottom-left
            $nx = $x - 1;
            $ny = $y + 1;
            if (self::isNeighbourAlive($nx, $ny)) {
                $count++;
            }
            
            // bottom-mid
            $nx = $x;
            $ny = $y + 1;
            if (self::isNeighbourAlive($nx, $ny)) {
                $count++;
            }
            
            // bottom-right
            $nx = $x + 1;
            $ny = $y + 1;
            if (self::isNeighbourAlive($nx, $ny)) {
                $count++;
            }
            
            if (self::$debug) {
                echo "$k($count), ";
            }
            self::$cells[$k]->neighbours = $count;
            self::setNextState(self::$cells[$k]);
        }
        self::iterateStates();
    }

    protected function heatDeathOfTheUniverse($cell) {

        // randomly kill living cells
        if ($cell->state) {
            $cell->state = (bool)rand(0,1);
        }
        return $cell;

    }

    public function setNextState($cell) {
        $cell->nextState = false;
        # 1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
        if ($cell->neighbours < 2 && $cell->state == true) {
            $cell->nextState = false;
            return;
        }
        # 2. Any live cell with two or three live neighbours lives on to the next generation.
        if (($cell->neighbours == 2 || $cell->neighbours == 3) && $cell->state == true) {
            $cell->nextState = true;
            return;
        }
        # 3. Any live cell with more than three live neighbours dies, as if by overpopulation.
        if ($cell->neighbours > 3 && $cell->state == true) {
            $cell->nextState = false;
            return;
        }
        # 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
        if ($cell->neighbours == 3 && $cell->state == false) {
            $cell->nextState = true;
            return;
        }
    }

    public function iterateStates() {
        foreach(self::$cells as $k => $v) {
            self::$cells[$k]->state = self::$cells[$k]->nextState;
        }
    }

    public function inBounds($int) {
        if ($int >= 0 && $int < self::$size) {
            return true;
        }
        return false;
    }

    public function isNeighbourAlive($nx, $ny) {
        if (self::inBounds($nx) && self::inBounds($ny)) {
            $newKey = floor($ny.$nx);
            if (self::$cells[$newKey]->state) {
                return true;
            }
        }
        return false;
    }

}
