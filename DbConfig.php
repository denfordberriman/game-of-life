<?php
class DbConfig {
 
    private $host = 'localhost';
    private $username = 'dev';
    private $password = 'dev';
    private $database = 'gol';
 
    protected $connection;
 
    public function __construct() {
 
        if (!isset($this->connection)) {

            // mysqli
            $this->connection = new mysqli($this->host, $this->username, $this->password, $this->database);
            
            // attempt connection
            if (!$this->connection) {
                throw new Exception("Unable to connect to database");
                exit;
            }            
        }    
 
        return $this->connection;
    }
}
