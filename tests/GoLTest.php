<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class GoLTest extends TestCase
{
    private static $universe;
    public static function setUpBeforeClass(): void
    {
        require_once 'GoL.php';
        SELF::$universe = new Universe();
    }

    // inBounds()
    public function testinBoundsNegativeInt(): void
    {
        $this->assertEquals(false, self::$universe->inbounds(-5));
    }
    public function testinBoundsPositiveInBoundsInt(): void
    {
        $this->assertEquals(true, self::$universe->inbounds(5));
    }
    public function testinBoundsPositiveOutOfBoundsInt(): void
    {
        $this->assertEquals(false, self::$universe->inbounds(55));
    }

    
}