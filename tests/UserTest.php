<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase
{
    private static $user;
    public static function setUpBeforeClass(): void
    {
        require_once 'User.php';
        SELF::$user = new User();
    }
    
    public function testCleanStripsTags(): void
    {
        $this->assertEquals("nor", self::$user->clean("<h1>nor</h1>"));
    }
}
