<?php
    session_start();
    
    // are we logged in?
    if(isset($_SESSION['user'])){
        header('location:index.php');
    }

    // process login attempt
    if (isset($_POST['username']) && isset($_POST['password'])) {
        // let's get ready to log in
        require "../User.php";
        $user = new User();
        // clean the inputs
        $username = $user->clean($_POST['username']);
        $password = $user->clean($_POST['password']);

        // do they check out?
        $check = $user->check_login($username, $password);
        if ($check) {
            // set user session
            $_SESSION["user"] = $check;
            // redirect
            header('location:index.php');
        } else {
            $error = "Invalid login";
        }
            
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Game of life</title>
   
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Baloo+Bhaina+2:wght@800&family=Dongle&display=swap');
    </style>

    <link rel="stylesheet" type="text/css" href="./styles/css/gol.min.css">
    
</head>
<body>
    <header>
        <h1>Game of life</h1>
    </header>

    <main>
        <form method="POST" action="login.php">
		    <fieldset>
                <?php
                    if (isset($error)) {
                        echo "<div class='error'>$error</div>";
                    }
                ?>
                <legend>Log in to carry on</legend>
                <div>
                    <label>Username</label>
		            <input placeholder="Username" type="text" name="username" autofocus required>
                </div>
                <div>
                    <label>Password</label>
		            <input placeholder="Password" type="password" name="password" required>
                </div>
		       	<button type="submit" name="login">Log in</button>
            </fieldset>
        </form>
    </main>
    <footer>
        A 🐘PHP exercise by denford 
    </footer>
</body>
</html>