<?php
    // fetch the universe
    require "../GoL.php";
	session_start();

    // are we logging out?
    if (isset($_POST['logout'])) {
        unset($_SESSION);
        session_destroy();
        session_write_close();
    }

    // reset gol
    if (isset($_POST['reset'])) {
        unset($_SESSION['cells']);
        unset($_SESSION['iteration']);
    }

	// are we logged in?
	if (!isset($_SESSION['user'])) {
		header('location:login.php');
	}

    

    if (!isset($_SESSION['cells'])) {
        $universe = new Universe();
        $universe->createUniverse();
        
        //print_r($universe->cells);
        $cells = $universe->getUniverseHTML();
        
        // stash info for next page load
        $iteration = 1;
        $_SESSION['iteration'] = $iteration;
        $_SESSION['cells'] = serialize($universe::$cells);
    } else {

        $universe = new Universe();
        $universe::$cells = unserialize($_SESSION['cells']);
        $universe->runSimulation();
        $cells = $universe->getUniverseHTML();
        
        $_SESSION['iteration'] = $_SESSION['iteration'] + 1;
        $iteration = $_SESSION['iteration'];
        $_SESSION['cells'] = serialize($universe::$cells);
    }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Conways - Game of life</title>
   
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Baloo+Bhaina+2:wght@800&family=Dongle&display=swap');
    </style>

    <link rel="stylesheet" type="text/css" href="./styles/css/gol.min.css">
    
</head>
<body>
    <header>
        <h1>Conways - Game of life</h1>
        
        <form class="logout" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <button type="submit" name="logout" value="logout">log out</button>
        </form>
        
    </header>

    <main>
        <div class="universe">
            <?php print_r($cells); ?>
        </div>
        <div class="row">
            <form class="next" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <button type="submit" name="next" value="next">Next</button>
            </form>
            <div class="iteration">iteration: <?php echo $iteration; ?></div>
            <form class="reset" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <button type="submit" name="reset" value="reset">Reset</button>
            </form>
        </div>
    </main>
    <footer>
        A 🐘PHP exercise by denford 
    </footer>
</body>
</html>